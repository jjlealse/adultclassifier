import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

public class ResizeImagesMain {
	
	private static final String DIRECTORY_MAIN = "/home/joao/Área de Trabalho/Curso_Udemy/adult_classifier/";
	
	public static void main(String[] args) {
		try {
			String pathPorn = DIRECTORY_MAIN + "porn";
			String pathNormal = DIRECTORY_MAIN + "normal";
			
			String pathPornResize = DIRECTORY_MAIN + "porn_resize";
			String pathNormalResize = DIRECTORY_MAIN + "normal_resize";
			
			File filePorn = new File(pathPorn);
			BufferedImage imgBuff = ImageIO.read(filePorn.listFiles()[0]);
			
			BufferedImage imgResized = resize(imgBuff, 200, 200);
			
			File fileResized = new File(pathPornResize + "/" + filePorn.listFiles()[0].getName());
			ImageIO.write(imgResized, "jpg", fileResized);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private static BufferedImage resize(BufferedImage img, int height, int width) {
		Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = resized.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
		return resized;
	}

}
