import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;

import javax.imageio.ImageIO;

import net.semanticmetadata.lire.imageanalysis.BinaryPatternsPyramid;
import net.semanticmetadata.lire.imageanalysis.EdgeHistogram;
import net.semanticmetadata.lire.imageanalysis.JpegCoefficientHistogram;
import net.semanticmetadata.lire.imageanalysis.LireFeature;
import net.semanticmetadata.lire.imageanalysis.PHOG;
import weka.classifiers.trees.RandomForest;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.converters.ArffLoader.ArffReader;

public class TesteAdultClassifier {
	
	private static final String DIRECTORY_MAIN = "/home/joao/Área de Trabalho/Curso_Udemy/adult_classifier/";
	private static final String PATH_MODEL_RF = DIRECTORY_MAIN + "random_forest_binaryFilter.model";
	private static final String PATH_MODEL_RF_JPEG_FILTER = DIRECTORY_MAIN + "modelo_classificador_novo.model";
	private static final String PATH_MODEL_SMO = DIRECTORY_MAIN + "adult_classifier_svm.model";
	private static final String PATH_TESTE_IMAGES = DIRECTORY_MAIN + "testeImages/t9.jpg";
	
	public static void main(String[] args) {
		try {
//			RandomForest randomForest = (RandomForest) SerializationHelper.read(new FileInputStream(PATH_MODEL_RF));
//			SMO smo = (SMO) SerializationHelper.read(new FileInputStream(PATH_MODEL_SMO));
			RandomForest randomForest = (RandomForest) SerializationHelper.read(new FileInputStream(PATH_MODEL_RF_JPEG_FILTER));
			
			Instance instance = createInstanceTeste();
			
			//double[] d = randomForest.distributionForInstance(instance);
			//double[] d = smo.distributionForInstance(instance);
			double[] d = randomForest.distributionForInstance(instance);
			
			double percentPositive = d[0] * 100;
			double percentNegative = d[1] * 100;
			
			if (percentPositive > percentNegative && percentPositive >= 65) {
				System.out.println("É pornografia com " + percentPositive + " de confiança.");
			} else {
				System.out.println("Não é pornografia com " + percentNegative + " de confiança.");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static LireFeature getPHOGFilter() {
		PHOG phog = new PHOG();
		phog.extract(getImageByDirectory(PATH_TESTE_IMAGES));
		return (LireFeature) phog;
	}
	
	public static LireFeature getJpegCoeficientFilter() {
		JpegCoefficientHistogram jpeg = new JpegCoefficientHistogram();
		jpeg.extract(getImageByDirectory(PATH_TESTE_IMAGES));
		return (LireFeature) jpeg;
	}
	
	public static LireFeature getLireFeatureEdgeHistogram() {
		EdgeHistogram edge = new EdgeHistogram();
		edge.extract(getImageByDirectory(PATH_TESTE_IMAGES)); 
		return (LireFeature) edge;
	}
	
	public static LireFeature getLireFeatureBinaryPyramidFilter() {
		BinaryPatternsPyramid binary = new BinaryPatternsPyramid();
		binary.extract(getImageByDirectory(PATH_TESTE_IMAGES));
		return (LireFeature) binary;
	}
	
	public static Instance createInstanceTeste() {
		//LireFeature lireFeature = getLireFeatureEdgeHistogram();
		//LireFeature lireFeature = getLireFeatureBinaryPyramidFilter();
		LireFeature lireFeature = getJpegCoeficientFilter();
		double[] features = lireFeature.getDoubleHistogram();
		int numFeatures = lireFeature.getDoubleHistogram().length;
		String featureName = lireFeature.getFeatureName();

		/**
		FastVector fastVector = new FastVector(numFeatures + 1);
		
		double[] values = new double[numFeatures];
		
		for (int index = 0; index < features.length; index++) {
			Attribute att = new Attribute(featureName + index);
			fastVector.addElement(att);
			
//			values[index] = features[index];
		}

		fastVector.addElement(new Attribute("class", true));
		*/
		
		Instances instances = loadArffFile();
		instances.setClassIndex(instances.numAttributes() - 1);

		Instance instance = new DenseInstance(numFeatures);

		instance.setDataset(instances);
		for (int index = 0; index < features.length; index++) {
			instance.setValue(index, features[index]);
		}
		instances.add(instance);
		
		return instance;
	}
	
	private static BufferedImage getImageByDirectory(String dirImage) {
		try {
			BufferedImage img = ImageIO.read(new File(dirImage));
			return img;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private static Instances loadArffFile() {
		try {
			String path = DIRECTORY_MAIN + "adult_classifier_jpeg_filters.arff";
			BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
			ArffReader arff = new ArffReader(reader);
			Instances data = arff.getStructure();
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

}
