import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Main {
	
	private static final String DIRECTORY_MAIN = "/home/joao/classificador/adult_classifier/";
	
	public static void main(String[] args) {
		try {
			String pathPorn = DIRECTORY_MAIN + "porn";
			String pathNormal = DIRECTORY_MAIN + "normal";
			
			File filePorn = new File(pathPorn);
			File fileNormal = new File(pathNormal);
			
			createArff(filePorn.listFiles(), fileNormal.listFiles());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void createArff(File[] filesPorn, File[] filesNoPorn) throws IOException {
		File fileArff = new File(DIRECTORY_MAIN + "adult_classifier.arff");
		StringBuilder sb = new StringBuilder();
		sb.append("@relation adult_images_classifier").append(System.getProperty("line.separator"));
		sb.append("@attribute filename string").append(System.getProperty("line.separator"));
		sb.append("@attribute class {S, N}").append(System.getProperty("line.separator"));
		sb.append("@data").append(System.getProperty("line.separator"));
		int count = 0;
		for (File fPorn : filesPorn) {
			sb.append(fPorn.getName()).append(",").append("S").append(System.getProperty("line.separator"));
			checkImageFile(fPorn);
			count++;
			System.out.println("Processando porno: " + count);
		}
		
		count = 0;
		for (File fNoPorn : filesNoPorn) {
			sb.append(fNoPorn.getName()).append(",").append("N").append(System.getProperty("line.separator"));
			checkImageFile(fNoPorn);
			count++;
			System.out.println("Processando normal: " + count);
		}
		
		FileUtil.writerFileByListString(fileArff, sb.toString());
	}
	
	private static void checkImageFile(File image) {
		try {
			ImageIO.read(image);
		} catch (Exception e) {
			deleteFile(image);
			deleteFile(new File(DIRECTORY_MAIN + "pornAndNormal/" + image.getName()));
			System.out.println("Arquivo com erro: " + image.getName());
		}
	}
	
	private static void deleteFile(File image) {
		image.delete();
	}

}
