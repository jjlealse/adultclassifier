import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Random;

import weka.classifiers.Evaluation;
import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.core.SerializationHelper;

public class GenerateModelMain {

	private static final String PATH_ARFF_DATA_SET = "/home/joao/Área de Trabalho/Curso_Udemy/adult_classifier/adult_classifier_jpeg_filters.arff";
	private static final String PATH_SAVED_MODEL = "/home/joao/Área de Trabalho/Curso_Udemy/adult_classifier/modelo_classificador_novo.model";
	
	public static void main(String[] args) {
		try {
			File fileArff = new File(PATH_ARFF_DATA_SET);
			BufferedReader reader = new BufferedReader(new FileReader(fileArff));
			Instances instanceTrain = new Instances(reader);
			instanceTrain.setClassIndex(instanceTrain.numAttributes() - 1);
			reader.close();

			RandomForest randomForest = new RandomForest();
			randomForest.buildClassifier(instanceTrain);

			Evaluation eval = new Evaluation(instanceTrain);
			eval.crossValidateModel(randomForest, instanceTrain, 10, new Random(1));

			System.out.println(eval.toSummaryString("\n Resultado \n=====\n", true));
			System.out.println(eval.fMeasure(1) + " " + eval.precision(1) + " " + eval.recall(1) + " ");
			
			SerializationHelper.write(PATH_SAVED_MODEL, randomForest);
			System.out.println("Modelo salvo com sucesso");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
