import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;

public class FileUtil {
	
	private static final String ENCODING = "ISO-8859-1";
	
	public static void writerFileByListString(File file, List<String> strings) throws IOException {
		OutputStreamWriter outputWriter = new OutputStreamWriter(new FileOutputStream(file), ENCODING);
		PrintWriter out = new PrintWriter(new BufferedWriter(outputWriter));
		try {
			for (String string : strings) {
				out.println(string);
			}
		} finally {
			out.close();
		}
	}
	
	public static void writerFileByListString(File file, String str) throws IOException {
		OutputStreamWriter outputWriter = new OutputStreamWriter(new FileOutputStream(file), ENCODING);
		PrintWriter out = new PrintWriter(new BufferedWriter(outputWriter));
		try {
			out.println(str);
		} finally {
			out.close();
		}
	}
	
}
